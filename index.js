#!/usr/bin/env node

import chalk from 'chalk';
import clear from 'clear';
import figlet from 'figlet';
import program from 'commander';

import {
  login,
  logout,
  list,
  install,
  publish,
  publishedStatus,
} from './lib/commands';

clear();

console.log(
  `${chalk.yellow(
    figlet.textSync('OpenBits', { horizontalLayout: 'full' }),
  )}\n`,
);

const run = async () => {
  program
    .command('login <arweave-key-store>')
    .description('Login into OpenBits with an arweave key store')
    .action(login);
  program
    .command('logout')
    .description('Logout from OpenBits')
    .action(logout);
  program
    .command('list')
    .description('List all the published OpenBits')
    .action(list);
  program
    .command('publish')
    .description('Publish the OpenBit from the Current Working Directory')
    .action(publish);
  program
    .command('install <openbit-name@openbit-version>')
    .description('Install an OpenBit. If no version is provided the latest one will be installed.')
    .option('--save-dev', 'Install the OpenBit only in the dev environment')
    .action(install);
  program
    .command('published-status <openbit-name@openbit-version>')
    .description('Check the status of openbits that you have published')
    .action(publishedStatus);

  program.parse(process.argv);
};

run();
