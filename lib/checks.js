import { directoryExists } from './files';
import configStore from './config-store';

export const isNodePackage = () => {
  if (!directoryExists('package.json')) {
    return false;
  }
  return true;
};

export const userKey = () => (
  configStore().get('arweave-key')
);

// check if the user has enough AR to install an OpenBit
export const userHasArToInstall = (amount, reward) => {
  const feeTotal = configStore().get('OpenBitsFee') + configStore().get('InstallationFee') + reward;
  return amount > feeTotal;
};
