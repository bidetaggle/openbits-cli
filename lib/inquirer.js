import inquirer from 'inquirer';

export const askForOpenBitPayment = async (randWord, totalCost) => {
  const questions = [
    {
      name: 'payOpenBit',
      type: 'input',
      message: `🚀🪐🚀 Do you want to pay ${totalCost} for installing this OpenBit and contribute a donation to the MULTIVERSE? If so, write ${randWord} 🚀🪐🚀`,
    },
  ];
  return inquirer.prompt(questions);
};

export const askForTargetProfit = async () => {
  const questions = [
    {
      name: 'targetProfit',
      type: 'input',
      message: '💰 What is your target profit for this OpenBit (min 1 AR)? 💰',
      validate: (val) => (Number(val) >= 1),
    },
  ];
  return inquirer.prompt(questions);
};

export const askForAllowInvestments = async () => {
  const questions = [
    {
      name: 'allowInvestments',
      type: 'confirm',
      default: true,
      message: '💰 Would you like to allow 4 investors to buy 5 shares each as described in the table above? 💰',
    },
  ];
  return inquirer.prompt(questions);
};

export const askForConfirmPublication = async (randWord, totalCost) => {
  const questions = [
    {
      name: 'confirmPublication',
      type: 'input',
      message: `🚀🪐🚀 Do you want to pay ${totalCost} to publish this OpenBit? If so write ${randWord} 🚀🪐🚀`,
    },
  ];
  return inquirer.prompt(questions);
};
